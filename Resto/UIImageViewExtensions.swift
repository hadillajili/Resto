//
//  UIImageViewExtensions.swift
//  Resto
//
//  Created by Houcem Soued on 31/07/2018.
//  Copyright © 2018 proxym-it. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

/*enum ImageType{
    case selectedImage
    case disabledImage
}*/

extension UIImageView {
    

    func setImage(url : String? /*, placeholder : String? , type : ImageType*/){
        //placeholder: au cas ou url introuvable on le remplace par l image dont le nom est ""
        // type image: selcted,disabled... l'appel se fait exp: setImage("http...","image1", .selected)
//        let url = URL(string: url ?? "")
        
        self.sd_setImage(with: URL(string: url ?? ""), placeholderImage: #imageLiteral(resourceName: "holder"))
//         if url == nil{
//            let placeholderImage = UIImage(named: "holder.png")
//            self.image = placeholderImage
//         }else{
//            self.sd_setImage(with:  URL(string: url!)  )
//
//        } equivalent a         self.sd_setImage(with: URL(string: url ?? ""), placeholderImage: #imageLiteral(resourceName: "holder"))

    }
    
}
