//
//  ViewControllerTableViewCell.swift
//  Resto
//
//  Created by hadil on 03/08/2018.
//  Copyright © 2018 proxym-it. All rights reserved.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var imageEffect: UIImageView!
    @IBOutlet weak var imageResto: UIImageView!
    @IBOutlet weak var labelText: UILabel!
   
    @IBOutlet weak var mapButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func draw(_ rect: CGRect) {
        mapButton.layer.cornerRadius = mapButton.frame.height/2
        imageResto.layer.cornerRadius = 20
        imageEffect.layer.cornerRadius = 20
        imageEffect.clipsToBounds = true 
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
