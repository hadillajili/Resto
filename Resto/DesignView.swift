//
//  DesignView.swift
//  Resto
//
//  Created by hadil on 16/08/2018.
//  Copyright © 2018 proxym-it. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class DesignView : UIView{
    @IBInspectable var cornerRadius: CGFloat = 20
    
    override func layoutSubviews() {
       layer.cornerRadius=cornerRadius
    }
}
